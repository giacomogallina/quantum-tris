use std::collections::BTreeMap;
use std::io::{ self, stdout };
use std::error::Error;

use rand::prelude::*;

use crossterm::{
    style::{ Color, Stylize }, execute, terminal::{ self, Clear, ClearType },
    cursor::{ MoveTo, MoveToColumn, MoveToNextLine, Hide, position },
    event::{ self, Event, MouseEvent, MouseEventKind, MouseButton, KeyEvent, KeyCode, KeyModifiers }
};

#[derive(Copy, Clone, Debug)]
enum Player {
    X,
    O,
}

fn color(p: Player) -> Color {
    match p {
        Player::X => Color::DarkRed,
        Player::O => Color::Yellow,
    }
}

fn other_player(p: Player) -> Player {
    match p { Player::X => Player::O, Player::O => Player::X }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
struct Tris {
    x: u16,
    o: u16,
}

impl Tris {
    fn new() -> Self {
        Tris {
            x: 0,
            o: 0,
        }
    }

    fn add(&self, i: usize, p: Player) -> Self {
        match p {
            Player::X => Tris{ x: self.x | ((1 << i) & !self.o), o: self.o },
            Player::O => Tris{ o: self.o | ((1 << i) & !self.x), x: self.x },
        }
    }

    fn check_winner(&self, p: Player) -> bool {
        let b = match p {
            Player::X => self.x,
            Player::O => self.o,
        };
        (b & (b >> 1) & (b >> 2)) | (b & (b >> 3) & (b >> 6)) |
        (b & (b >> 4) & (b >> 8)) | ((b >> 2) & (b >> 4) & (b >> 6) & 1) != 0
    }

    fn is_compatible(&self, t: Tris, s: u16) -> bool {
        (self.x & s == t.x & s) && (self.o & s == t.o & s)
    }

    fn is_full(&self) -> bool {
        self.x | self.o == 0b111111111
    }
}

enum Move {
    Insert(Player, u16),
    Collapse(u16),
}

struct QTris {
    probs: BTreeMap<Tris, f32>,
}

impl QTris {
    fn new() -> Self {
        let mut probs = BTreeMap::new();
        probs.insert(Tris::new(), 1.0);
        QTris { probs }
    }

    fn random(&self) -> Tris {
        let mut x: f32 = random();
        for (tris, pr) in self.probs.iter() {
            if *pr >= x {
                return *tris;
            }
            x -= pr;
        }
        *self.probs.keys().last().expect("QTris had no states!")
    }

    fn make_move(&mut self, m: Move) {
        let mut new_probs = BTreeMap::new();
        match m {
            Move::Insert(pl, s) => {
                let n = s.count_ones() as f32;
                for (tris, pr) in self.probs.iter() {
                    for i in 0..9 {
                        if (s & (1 << i)) != 0 {
                            let nt = tris.add(i, pl);
                            new_probs.insert(nt, new_probs.get(&nt).unwrap_or(&0.0) + pr / n);
                        }
                    }
                }
            },
            Move::Collapse(s) => {
                let t = self.random();
                let p: f32 = self.probs.iter().filter_map(|(tris, pr)| if t.is_compatible(*tris, s) { Some(pr) } else { None }).sum();
                for (tris, pr) in self.probs.iter() {
                    if t.is_compatible(*tris, s) {
                        new_probs.insert(*tris, pr/p);
                    }
                }
            },
        }
        self.probs = new_probs;
    }

    fn winner(&self) -> Option<Player> {
        let xw = self.probs.keys().all(|t| t.check_winner(Player::X) && !t.check_winner(Player::O));
        let ow = self.probs.keys().all(|t| t.check_winner(Player::O) && !t.check_winner(Player::X));
        match (xw, ow) {
            (true, false) => Some(Player::X),
            (false, true) => Some(Player::O),
            _ => None,
        }
    }

    fn check_end(&self) -> bool {
        self.probs.keys().all(|t| t.is_full() && !(t.check_winner(Player::X) ^ t.check_winner(Player::O)) ) ||
            self.winner().is_some()
    }
}

fn write_with_blocks(x: f32, l: usize) -> String {
    let blocks = [" ", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"];
    let x = x.clamp(0.0, 1.0);
    let mut res = String::new();
    for i in 0..l {
        res += blocks[((x * l as f32 - i as f32).clamp(0.0, 1.0) * 8.0).round() as usize];
    }
    res
}

fn print_qtris(qt: &QTris, p: Player, ss: u16) -> Result<(), Box<dyn Error>> {
    let nums = ["¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹"];
    let stats: Vec<Vec<f32>> = vec![Player::X, Player::O].iter().map(|p| {(0..9).map(|i| {
        qt.probs.iter().filter_map(|(tris, pr)|
            if tris.is_compatible(Tris::new().add(i, *p), 1 << i) {
                Some(pr)
            } else {
                None
            }
        ).sum::<f32>()
    }).collect()}).collect();
    let x_stats = &stats[0];
    let o_stats = &stats[1];

    let (col, _) = position()?;
    let col = col + 1;

    for i in 0..3 {
        if i > 0 {
            print!("________|________|________");
            execute!(stdout(), MoveToNextLine(1), MoveToColumn(col))?;
        }
        for j in 0..3 {
            if j > 0 { print!("|"); }
            print!("{}       ", if ss & (1 << (3*i+j)) != 0 { nums[3*i+j].black().on(color(p)) } else { nums[3*i+j].white() });
        }
        execute!(stdout(), MoveToNextLine(1), MoveToColumn(col))?;
        for j in 0..3 {
            if j > 0 { print!("|"); }
            print!(" {} ", write_with_blocks(x_stats[3*i+j], 6).with(color(Player::X)).on_dark_grey());
        }
        execute!(stdout(), MoveToNextLine(1), MoveToColumn(col))?;
        for j in 0..3 {
            if j > 0 { print!("|"); }
            print!(" {} ", write_with_blocks(o_stats[3*i+j], 6).with(color(Player::O)).on_dark_grey());
        }
        execute!(stdout(), MoveToNextLine(1), MoveToColumn(col))?;
    }
    print!("        |        |        ");
    execute!(stdout(), MoveToNextLine(1))?;
    Ok(())
}

fn play_qtris() -> Result<(), Box<dyn Error>> {
    let mut offset = (terminal::size()?.0 - 26) / 2;
    terminal::enable_raw_mode()?;
    execute!(stdout(), event::EnableMouseCapture, Hide)?;
    let mut current_player = Player::O;
    let mut state = QTris::new();
    while !state.check_end() {
        let mut ss = 0;
        let mut needs_redraw = true;
        current_player = other_player(current_player);
        let mov = loop {
            if needs_redraw {
                execute!(io::stdout(), Clear(ClearType::All), MoveTo(offset, 1))?;
                print!("     It's {} turn", "██████".with(color(current_player)));
                execute!(stdout(), MoveToNextLine(2), MoveToColumn(offset+1))?;
                print_qtris(&state, current_player, ss)?;
            }
            needs_redraw = false;
            match event::read()? {
                Event::Key(KeyEvent { code: KeyCode::Char('c'), modifiers: KeyModifiers::CONTROL }) => {
                    return Ok(());
                },
                Event::Key(KeyEvent { code: KeyCode::Char(c), .. }) => {
                    if c >= '1' && c <= '9' {
                        ss ^= 1 << (c as u8 - '1' as u8);
                        needs_redraw = true;
                    }
                },
                Event::Key(KeyEvent { code: KeyCode::Enter, .. }) => {
                    if ss > 0 {
                        break Move::Insert(current_player, ss);
                    }
                },
                Event::Key(KeyEvent { code: KeyCode::Backspace, .. }) => {
                    if ss > 0 {
                        break Move::Collapse(ss);
                    }
                },
                Event::Resize(cols, _) => {
                    offset = (cols - 26) / 2;
                    needs_redraw = true;
                },
                Event::Mouse(MouseEvent { kind: MouseEventKind::Down(MouseButton::Left), column: c, row: r, .. }) => {
                    if c >= offset && c < offset+26 && (c-offset+1) % 9 != 0 && r >= 3 && r < 15 {
                        let i = ((r - 3) as f32 / 11.0 * 3.0).clamp(0.0, 2.0).floor() as usize;
                        let j = ((c - offset) as f32 / 26.0 * 3.0).clamp(0.0, 2.0).floor() as usize;
                        ss ^= 1 << (3*i + j);
                        needs_redraw = true;
                    }
                }
                _ => (),
            }
        };
        state.make_move(mov);
    }
    execute!(io::stdout(), Clear(ClearType::All), MoveTo(offset, 1))?;
    if let Some(winner) = state.winner() {
        print!("          {} WON!", "██████".with(color(winner)));
    } else {
        print!("           DRAW");
    }
    execute!(stdout(), MoveToNextLine(2), MoveToColumn(offset+1))?;
    print_qtris(&state, current_player, 0)?;
    execute!(stdout(), MoveToNextLine(1))?;
    Ok(())
}

fn main() {
    play_qtris().unwrap();
}
